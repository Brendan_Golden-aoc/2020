
pub fn input_generator(input: &str) -> Vec<Vec<i8>> {
    let mut result: Vec<Vec<i8>> = Vec::new();

    for line in input.split("\n") {
        let row: Vec<i8> = line
            .split("")
            .filter(|x| *x != "")
            .map(|x| {
                if x == "." {
                    return 0
                }
                if x == "#" {
                    return 1
                }
                return 1
            })
            .collect();


        result.push(row)


    }
    result
}

pub struct Slope {
    pub right: usize,
    pub down: usize
}

pub fn solve_part1(input: &Vec<Vec<i8>>) -> i32 {
    let slope = Slope { right: 3, down: 1 };

    get_trees (input, slope)
}

fn get_trees (input: &Vec<Vec<i8>>, slope: Slope) -> i32{
    let mut trees = 0;

    for i in 0..input.len() {
        let row = i * slope.down;
        let col = i * slope.right;

        if row > input.len() { continue }
        // first row is at &input[0]
        let row_data = &input[row];

        // get teh position in the vec
        let row_data_position = col % row_data.len();

        trees += row_data[row_data_position] as i32;

    }

    trees

}

pub fn solve_part2(input: &Vec<Vec<i8>>) -> u64 {
    let path_1 = get_trees (&input, Slope { right: 1, down: 1 }) as u64;
    let path_2 = get_trees (&input, Slope { right: 3, down: 1 }) as u64;
    let path_3 = get_trees (&input, Slope { right: 5, down: 1 }) as u64;
    let path_4 = get_trees (&input, Slope { right: 7, down: 1 }) as u64;
    let path_5 = get_trees (&input, Slope { right: 1, down: 2 }) as u64;

    path_1 * path_2 * path_3 * path_4 * path_5
}