mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;


aoc::main! {
    year 2020;
    day01 : input_generator => solve_part1, solve_part2;
    day02 : input_generator => solve_part1, solve_part2;
    day03 : input_generator => solve_part1, solve_part2;
    day04 : input_generator => solve_part1, solve_part1_v2, solve_part2, solve_part2_v2;
    day05 : input_generator => solve_part1, solve_part2;
    day06 : input_generator => solve_part1, solve_part2;
    day07 : input_generator => solve_part1, solve_part2;
    day08 : generator => part_1, part_2;
    day09 : generator => part_1, part_2;
    day10 : generator => test, part_1, part_2;
    day11 : generator => test, part_1, part_2;
    day12 : generator => test, part_1;
}