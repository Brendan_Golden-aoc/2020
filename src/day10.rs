pub fn generator(input: &str) -> Vec<i32> {
    let mut result: Vec<i32> = Vec::new();

    for line in input.split("\n") {
        let number = line.parse::<i32>();
        if number.is_err(){continue}
        result.push( number.unwrap())
    }
    result.sort();

    result
}


pub fn part_1(bag:  &Vec<i32>) -> i32 {
    let mut jolts = 0;
    let (mut difference_1, mut _difference_2, mut difference_3, mut _difference_other) = (0,0,0,0);

    for adapter in bag {
        match adapter - &jolts {
            1 => { difference_1 +=1 },
            2 => { _difference_2 +=1 },
            3 => { difference_3 +=1 },
            _ => { _difference_other +=1 }
        }

        jolts = *adapter;
    }
    // device is always +3 above teh adapters
    //jolts += 3;
    difference_3 +=1;

    //println!("1:{} 2:{} 3:{} other:{} jolts:{}, in bag:{}", difference_1, difference_2, difference_3, difference_other,jolts, bag.len());

    // multiply to get the final answer
    difference_1 * difference_3
}


pub fn test(_unused: &Vec<i32>) -> i32 {
    println!("Testing");

    let input1 = "16
10
15
5
1
11
7
19
6
12
4";

    let generated1 = generator(input1);
    let part_1_1 = part_1(&generated1);

    println!("Input 1, part 1. Expected: 35, Got: {}", part_1_1);
    let part_2_1 = part_2(&generated1);
    println!("Input 1, part 2. Expected: 8, Got: {}", part_2_1);

    let input2 = "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";

    let generated2 = generator(input2);
    let part_1_2 = part_1(&generated2);

    println!("Input 2, part 1. Expected: 220, Got: {}", part_1_2);

    let part_2_2 = part_2(&generated2);
    println!("Input 2, part 2. Expected: 19208, Got: {}", part_2_2);

    0
}


pub fn part_2(bag:  &Vec<i32>) -> usize {
    let mut jolts = 0;

    let mut broken: Vec<Vec<i32>> = Vec::new();
    let mut broken_sub: Vec<i32> = Vec::new();

    // put the wall jack intot eh equation
    broken_sub.push(0);

    for adapter in bag {
        match adapter - jolts {
            1 | 2 => {
                broken_sub.push(*adapter)
            },
            3 => {
                broken.push(broken_sub);
                broken_sub = Vec::new();
                broken_sub.push(*adapter);
            },
            _ => {  }
        }

        jolts = *adapter;
    }

    // make sure to add the last array to the list
    broken.push(broken_sub);

    let mut permutations: Vec<usize> = Vec::new();
    for sub in broken {
        // first and alst values are immutable
        // so if there are 2 or 1 numbvers ther permutations is 1
        match sub.len() {
            1 => { permutations.push(1) },
            2 => { permutations.push(1) },
            3 => {
                // XXX
                // XOX
                permutations.push(2)
            },
            4 => {
                // XXXX
                // XOXX
                // XXOX
                // XOOX
                permutations.push(4)
            },
            5 => {
                // XXXXX
                // XOXXX
                // XXOXX
                // XXXOX
                // XOOXX
                // XOXOX
                // XXOOX

                permutations.push(7)
            },
            _ => {println!("Error")}
        }
    }

    permutations.iter().product::<usize>()
}