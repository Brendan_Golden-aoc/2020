pub fn generator(input: &str) -> Vec<i32> {
    let mut result: Vec<i32> = Vec::new();

    for line in input.split("\n") {
        let number = line.parse::<i32>();
        if number.is_err(){continue}
        result.push( number.unwrap())
    }
    result
}

pub fn part_1(input:  &Vec<i32>) -> i32 {
    // pre amble is 25 numbers
    get_invalid(input).0
}

fn get_sum(target: &i32, values: &[i32]) -> bool {
    for i in values {
        for j in values {
            if &(i+j) == target {
                return true
            }
        }
    }
    return false
}

fn get_invalid (input:  &Vec<i32>) -> (i32, usize) {
    // pre amble is 25 numbers
    let preamble = 25;
    for (position, current_value) in input.iter().enumerate() {
        if position <= preamble {continue}
        let previous = &input[(position-preamble)..position];
        if !get_sum(current_value, previous) {
            return (*current_value, position)
        }
    }
    (0,0)
}

pub fn part_2(input:  &Vec<i32>) -> i32 {
    // pre amble is 25 numbers
    let invalid = get_invalid(input);

    let target = invalid.0 as i32;
    let input_trimmed = &input[..];


    // get rid of any value higher
    for (position, i) in input_trimmed.iter().enumerate(){
        if i >= &target {continue}

        let test_range = &input_trimmed[position..];
        let mut final_range : Vec<i32> = Vec::new();
        let mut sum = 0;
        for j in test_range {
            if j >= &target {continue}

            sum += *j;
            final_range.push(*j);

            if &sum > &target {
                break
            }

            if &sum == &target {
                let min = *final_range.iter().min().unwrap();
                let max = *final_range.iter().max().unwrap();

                return min+max
            }

        }
        if &sum > &target {
            continue
        }
    }

    0

}