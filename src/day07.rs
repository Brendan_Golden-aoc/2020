use std::collections::HashMap;

#[derive(Debug)]
pub struct Bag {
    // if the bag contains a shiney
    pub shiny: bool,

    // a hash of the sub bags and quantity
    pub bags_sub: HashMap<String, i64>
}

pub fn input_generator(input: &str) -> HashMap<String, Bag> {
    let mut result = HashMap::new();

    // new line seperates passports
    for line in input.split("\n") {
        let split:Vec<&str> = line.split(" bags contain ").collect();

        // this is the string that its indexed by
        let name = split[0];
        let contains  = split[1].replace(".", "").replace("bags", "").replace("bag", "");
        let bag_sub_raw:Vec<&str> = contains.trim().split(" , ").collect();

        let mut bags_sub = HashMap::new();

        let mut contains_shiney = false;
        for bag_sub in bag_sub_raw {
            if bag_sub == "no other" { continue }
            let partition = bag_sub.find(" ").unwrap();
            let number = bag_sub[..partition].parse::<i64>().unwrap();
            let bag_sub_name = &bag_sub[(partition+1)..];

            if bag_sub_name == "shiny gold" {
                contains_shiney = true;
            }

            bags_sub.insert(bag_sub_name.to_string(), number);
        }
        result.insert(name.to_string(), Bag {
            shiny: contains_shiney,
            bags_sub: bags_sub
        });
    }

    result
}

pub fn solve_part1(input: &HashMap<String, Bag>) -> i64 {
    let mut result = HashMap::new();

    for (name, details) in input.iter() {
        // has to be within a bag
        if name == "shiny gold" {continue}

        let bag = Bag {
            shiny: true,
            bags_sub: details.bags_sub.to_owned()
        };

        // no need to check, already contains it
        if details.shiny {
            result.insert(name.to_owned(),bag );
            continue
        }

        if traverse_tree(name, &input).0 {
            result.insert(name.to_owned(),bag );
        }

    }

    result.len() as i64
}


fn traverse_tree (name: &String, input: &HashMap<String, Bag>) -> (bool, i64) {
    let mut contains_shiney = false;
    let mut bags = 0;

    // bag we are searchiung
    let bag = input.get(name).unwrap();
    let sub_bags = &bag.bags_sub;

    for (sub_name, quantity) in sub_bags.iter() {
        let sub_bag = input.get(sub_name).unwrap();
        if sub_bag.shiny {
            contains_shiney = true;
        }
        // check any under it
        let traverse_result = traverse_tree(sub_name, input);
        if traverse_result.0 {
            contains_shiney = true;
        }

        bags += quantity + (quantity * traverse_result.1)
    }

    (contains_shiney,bags)
}

pub fn solve_part2(input: &HashMap<String, Bag>) -> i64 {
    traverse_tree(&"shiny gold".to_string(), &input).1
}