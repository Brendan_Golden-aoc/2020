
pub struct PasswordData {
    pub min: i32,
    pub max: i32,
    pub letter: String,
    pub password: String
}

pub fn input_generator(input: &str) -> Vec<PasswordData> {
    let mut result: Vec<PasswordData> = Vec::new();

    for line in input.split("\n") {
        let segments: Vec<&str> = line.split(" ").collect();

        if segments.len() != 3 { continue }

        let numbers: Vec<&str> = segments[0].split("-").collect();
        let min = numbers[0].parse::<i32>().unwrap();
        let max = numbers[1].parse::<i32>().unwrap();

        let letter = segments[1].replace(":", "");
        let password = segments[2].to_string();

        result.push(PasswordData {
            min: min,
            max: max,
            letter: letter,
            password: password
        } )
    }
    result
}

pub fn solve_part1(input: &Vec<PasswordData>) -> i32 {
    let mut valid = 0;

    for test in input {
        let count  = test.password.matches(&test.letter).count() as i32;

        if count >= test.min {
            if count <= test.max {
                valid += 1;
            }
        }

    }

    valid
}

pub fn solve_part2(input: &Vec<PasswordData>) -> i32 {
    let mut valid = 0;

    for test in input {
        let mut valid_sub = 0;

        let password: Vec<&str> = test.password.split("").collect();

        if password[test.min as usize] == &test.letter {
            valid_sub += 1;
        };

        if password[test.max as usize] == &test.letter {
            valid_sub += 1;
        };

        if valid_sub == 1 {
            valid += 1;
        }
    }

    valid
}

