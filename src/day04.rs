
pub fn input_generator(input: &str) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();

    // new line seperates passports
    for passport in input.split("\n\n") {
        // replace spaces with a new line to neatly format it

        let parsed = passport.replace(" ", "\n");

        result.push(parsed)
    }
    result
}

pub fn solve_part1(input: &Vec<String>) -> i32 {
    let mut valid = 0;


    for document in input {

        let mut byr = false;
        let mut iyr = false;
        let mut eyr = false;
        let mut hgt = false;
        let mut hcl = false;
        let mut ecl = false;
        let mut pid = false;

        //let mut cid = false;
        for line in document.split("\n") {
            // this checks if any of the core ones are present

            if line.contains("byr:") { byr = true; }
            if line.contains("iyr:") { iyr = true; }
            if line.contains("eyr:") { eyr = true; }
            if line.contains("hgt:") { hgt = true; }
            if line.contains("hcl:") { hcl = true; }
            if line.contains("ecl:") { ecl = true; }
            if line.contains("pid:") { pid = true; }


            //if line.contains("cid:") { cid = true; }
        }


        if byr && iyr && eyr && hgt && hcl && ecl && pid{
            valid +=1;
        }


    }


    valid
}

pub fn solve_part2(input: &Vec<String>) -> i32 {
    let mut valid = 0;


    for document in input {

        let mut byr = false;
        let mut iyr = false;
        let mut eyr = false;
        let mut hgt = false;
        let mut hcl = false;
        let mut ecl = false;
        let mut pid = false;

        //let mut cid = false;
        for line in document.split("\n") {
            // this checks if any of the core ones are present
            let data_raw: Vec<&str> = line.split(":").collect();
            let data = data_raw[1];

            if line.contains("byr:") {
                let number = data.parse::<i32>().unwrap();

                if number < 1920 {continue}
                if number > 2002 {continue}

                byr = true;

            }
            if line.contains("iyr:") {
                let number = data.parse::<i32>().unwrap();

                if number < 2010 {continue}
                if number > 2020 {continue}
                iyr = true;
            }
            if line.contains("eyr:") {
                let number = data.parse::<i32>().unwrap();

                if number < 2020 {continue}
                if number > 2030 {continue}

                eyr = true; }
            if line.contains("hgt:") {

                if data.contains("cm") {
                    let number = data.replace("cm", "").parse::<i32>().unwrap();
                    if number < 150 {continue}
                    if number > 193 {continue}

                    hgt = true;
                }

                if data.contains("in"){
                    let number = data.replace("in", "").parse::<i32>().unwrap();
                    if number < 59 {continue}
                    if number > 76 {continue}

                    hgt = true;
                }
            }
            if line.contains("hcl:") {

                if !data.contains("#") { continue }


                let mut len_total = 0;
                let mut len_valid = 0;

                for character in data.replace("#", "").split("").filter(|x| *x != ""){

                    len_total += 1;

                    let mut valid = false;

                    match character {
                        "0" => { valid = true },
                        "1" => { valid = true },
                        "2" => { valid = true },
                        "3" => { valid = true },
                        "4" => { valid = true },
                        "5" => { valid = true },
                        "6" => { valid = true },
                        "7" => { valid = true },
                        "8" => { valid = true },
                        "9" => { valid = true },
                        "a" => { valid = true },
                        "b" => { valid = true },
                        "c" => { valid = true },
                        "d" => { valid = true },
                        "e" => { valid = true },
                        "f" => { valid = true },
                        _ => {}
                    }

                    if valid {
                        len_valid += 1;
                    }
                }

                if len_valid != 6 {continue}
                if len_total != 6 {continue}

                hcl = true;
            }
            if line.contains("ecl:") {

                let mut valid = false;

                match data {
                    "amb" => { valid = true },
                    "blu" => { valid = true },
                    "brn" => { valid = true },
                    "gry" => { valid = true },
                    "grn" => { valid = true },
                    "hzl" => { valid = true },
                    "oth" => { valid = true },
                    _ => {}
                }

                if !valid {
                    continue
                }

                ecl = true;
            }
            if line.contains("pid:") {
                let mut len_total = 0;
                let mut len_valid = 0;

                for character in data.split("").filter(|x| *x != ""){
                    len_total += 1;

                    let mut valid = false;

                    match character {
                        "0" => { valid = true },
                        "1" => { valid = true },
                        "2" => { valid = true },
                        "3" => { valid = true },
                        "4" => { valid = true },
                        "5" => { valid = true },
                        "6" => { valid = true },
                        "7" => { valid = true },
                        "8" => { valid = true },
                        "9" => { valid = true },
                        _ => {}
                    }

                    if valid {
                        len_valid += 1;
                    }
                }
                if len_valid != 9 {continue}
                if len_total != 9 {continue}

                pid = true;
            }

            //if line.contains("cid:") { cid = true; }
        }


        if byr && iyr && eyr && hgt && hcl && ecl && pid{
            valid +=1;
        }


    }


    valid
}

pub fn solve_part1_v2(input: &Vec<String>) -> i32 {
    let mut valid = 0;

    for document in input {
        if  document.contains("byr:") &&
            document.contains("iyr:") &&
            document.contains("eyr:") &&
            document.contains("hgt:") &&
            document.contains("hcl:") &&
            document.contains("ecl:") &&
            document.contains("pid:")
        {
            valid +=1;
        }
    }

    valid
}

pub fn solve_part2_v2(input: &Vec<String>) -> i32 {
    let mut valid = 0;

    for document in input {
        let (mut byr, mut iyr, mut eyr, mut hgt, mut hcl, mut ecl, mut pid) = (false,false,false,false,false,false,false);

        //let mut cid = false;
        for line in document.split("\n") {
            // this checks if any of the core ones are present
            let data_raw: Vec<&str> = line.split(":").collect();
            let field = data_raw[0];
            let data = data_raw[1];

            match field {
                "byr" =>{
                    let number = data.parse::<i32>().unwrap();

                    if number < 1920 {break}
                    if number > 2002 {break}

                    byr = true;
                },
                "iyr" =>{
                    let number = data.parse::<i32>().unwrap();

                    if number < 2010 {break}
                    if number > 2020 {break}
                    iyr = true;
                },
                "eyr" =>{
                    let number = data.parse::<i32>().unwrap();

                    if number < 2020 {break}
                    if number > 2030 {break}

                    eyr = true;
                },
                "hgt" =>{
                    if data.contains("cm") {
                        let number = data.replace("cm", "").parse::<i32>().unwrap();
                        if number < 150 {break}
                        if number > 193 {break}

                        hgt = true;
                    }

                    if data.contains("in"){
                        let number = data.replace("in", "").parse::<i32>().unwrap();
                        if number < 59 {break}
                        if number > 76 {break}

                        hgt = true;
                    }
                },
                "hcl" =>{
                    if !data.contains("#") { break }
                    let mut len_total = 0;
                    let mut len_valid = 0;
                    for character in data.replace("#", "").chars(){

                        len_total += 1;
                        if character.is_alphanumeric() {
                            len_valid += 1;
                        }
                    }

                    if len_valid != 6 {break}
                    if len_total != 6 {break}

                    hcl = true;
                },
                "ecl" =>{
                    match data {
                        "amb" | "blu" | "brn" | "gry" | "grn" | "hzl"| "oth" => { },
                        _ => { break }
                    }

                    ecl = true;
                },
                "pid" =>{
                    let mut len_total = 0;
                    let mut len_valid = 0;

                    for character in data.chars(){
                        len_total += 1;
                        if character.is_digit(10)  {
                            len_valid += 1;
                        }
                    }
                    if len_valid != 9 {break}
                    if len_total != 9 {break}

                    pid = true;
                },

                _ => {}
            }
        }

        if byr && iyr && eyr && hgt && hcl && ecl && pid{
            valid +=1;
        }

    }
    valid
}