pub fn generator(input: &str) -> Vec<Vec<&str>> {
    let mut result: Vec<Vec<&str>> = Vec::new();

    for line in input.split("\n") {
        let row = line.split("").filter(|x| *x != "").collect::<Vec<&str>>();
        result.push(row)
    }

    result
}


pub fn part_1(seats:  &Vec<Vec<&str>>) -> i32 {
    // set initial state
    let mut seats_filled = 0;
    let mut seats_current = seats.to_owned();
    let mut seats_next: Vec<Vec<&str>> = Vec::new();

    loop {
        let mut changed = false;

        let mut seats_filled_tmp = 0;
        for (i,row) in seats_current.iter().enumerate() {
            let mut row_tmp: Vec<&str> = Vec::new();
            for (j,seat) in row.iter().enumerate() {
                // the floor remains the same throughout
                let adjacent = get_adjacent(&seats_current, i as i32, j as i32);

                match seat {
                    &"." => {
                        row_tmp.push(seat);
                    },
                    &"L" => {
                        if adjacent == 0 {
                            // if no occupied seats ajacent to it it becomes full (#)
                            row_tmp.push("#");
                            seats_filled_tmp += 1;
                            changed = true;
                        }else{
                            // else it remains L
                            row_tmp.push("L");
                        }
                    },
                    &"#" => {
                        if adjacent >= 4 {
                            // if 4+ ajacent seats are occupied this becomes L
                            row_tmp.push("L");
                            changed = true;
                        }else{
                            // else it remains #
                            row_tmp.push("#");
                            seats_filled_tmp += 1;
                        }

                    },
                    _ => {}
                }
            }
            seats_next.push(row_tmp)
        }

        //println!("run: {} {:?} changed: {}", iteration, &seats_current[0],  changed);

        // if reach stable state then break off

        if !changed {
            break
        }else{
            // set up the varibles for the next run
            seats_filled = seats_filled_tmp;
            seats_current = seats_next;
            seats_next = Vec::new();
        }
    }

    seats_filled
}


pub fn test(_unused: &Vec<Vec<&str>>) -> i32 {
    println!("Testing");

    let input1 = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";

    let generated1 = generator(input1);
    //println!("{:?}", &generated1);

    let part_1_1 = part_1(&generated1);

    println!("Input 1, part 1. Expected: 37, Got: {}", part_1_1);

    let part_1_2 = part_2(&generated1);

    println!("Input 1, part 2. Expected: 26, Got: {}", part_1_2);

    0
}


fn get_adjacent(seats:  &Vec<Vec<&str>>, i: i32, j: i32) -> i32{
    let mut seats_count= 0;

    // row above
    for i_tmp in (i-1)..=(i+1){
        if i_tmp < 0 {continue}
        if i_tmp >= seats.len() as i32 {continue}
        for j_tmp in (j-1)..=(j+1){
            if j_tmp < 0 {continue}
            if j_tmp >= seats[i_tmp as usize].len() as i32 {continue}

            // if its the core one
            if i_tmp == i && j_tmp == j {continue}

            if seats[i_tmp as usize][j_tmp as usize] == "#" {
                seats_count += 1;
            }
        }
    }

    seats_count
}

pub fn part_2(seats:  &Vec<Vec<&str>>) -> i32 {
    // set initial state
    let mut seats_filled = 0;
    let mut seats_current = seats.to_owned();
    let mut seats_next: Vec<Vec<&str>> = Vec::new();

    let mut _iteration = 0;
    loop {
        let mut changed = false;

        let mut seats_filled_tmp = 0;
        for (i,row) in seats_current.iter().enumerate() {
            let mut row_tmp: Vec<&str> = Vec::new();
            for (j,seat) in row.iter().enumerate() {
                // the floor remains the same throughout
                let adjacent = get_adjacent_2(&seats_current, i as i32, j as i32);

                match seat {
                    &"." => {
                        row_tmp.push(seat);
                    },
                    &"L" => {
                        if adjacent == 0 {
                            // if no occupied seats ajacent to it it becomes full (#)
                            row_tmp.push("#");
                            seats_filled_tmp += 1;
                            changed = true;
                        }else{
                            // else it remains L
                            row_tmp.push("L");
                        }
                    },
                    &"#" => {
                        if adjacent >= 5 {
                            // if 4+ ajacent seats are occupied this becomes L
                            row_tmp.push("L");
                            changed = true;
                        }else{
                            // else it remains #
                            row_tmp.push("#");
                            seats_filled_tmp += 1;
                        }

                    },
                    _ => {}
                }
            }
            seats_next.push(row_tmp)
        }


        /*
        println!();
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[0].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[1].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[2].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[3].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[4].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[5].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[6].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[7].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[8].join(""),  changed);
        println!("run: {} {:?} changed: {}", &_iteration, &seats_current[9].join(""),  changed);

         */

        // if reach stable state then break off

        //if iteration > 5 {break}

        if !changed {
            break
        }else{
            _iteration += 1;
            // set up the varibles for the next run
            seats_filled = seats_filled_tmp;
            seats_current = seats_next;
            seats_next = Vec::new();
        }
    }

    seats_filled
}

fn get_adjacent_2(seats:  &Vec<Vec<&str>>, i: i32, j: i32) -> i32{
    let mut seats_count= 0;

    // row above
    for i_tmp in (i-1)..=(i+1){
        if i_tmp < 0 {continue}
        if i_tmp >= seats.len() as i32 {continue}
        for j_tmp in (j-1)..=(j+1){
            if j_tmp < 0 {continue}
            if j_tmp >= seats[i_tmp as usize].len() as i32 {continue}

            // if its the core one
            if i_tmp == i && j_tmp == j {continue}

            let value = seats[i_tmp as usize][j_tmp as usize];

            /*
            if i == 1 && j ==0 {
                println!("i:{}, j:{} result: {}", &i_tmp, &j_tmp, &value );
            }
             */

            if value == "#" {
                seats_count += 1;
            }



            if value == "." {
                let i_offset = i_tmp - i;
                let j_offset = j_tmp - j;

                //println!("top {} {} {} {}",&i, &j, &i_offset, &j_offset);
                let nearest = get_nearest(seats, i_tmp, j_tmp, &i_offset, &j_offset);

                /*
                if i == 1 && j ==0 {
                    println!("  i:{}, j:{} result: {}", &i_tmp, &j_tmp, &nearest );
                }
                 */


                if nearest == "#" {
                    seats_count += 1;
                }
            }
        }
    }
    /*
    if i == 1 && j ==0 {
        println!("Seats around 1.0 {}", seats_count);
    }
     */

    seats_count
}

fn get_nearest(seats:  &Vec<Vec<&str>>, i: i32, j: i32, i_offset: &i32, j_offset: &i32) -> String {
    // check if i an j are valid
    // if not then return "."
    let default = ".".to_string();

    // check if I is in the valid range
    if i < 0 {return default}
    if i >= seats.len() as i32 {return default}

    let row = &seats[i as usize];

    // check if j is valid
    if j < 0 {return default}
    if j >= row.len() as i32 {return default}


    let value = row[j as usize];

    return if value == "." {
        let i_new = i + i_offset;
        let j_new = j + j_offset;


        get_nearest(seats, i_new, j_new, i_offset, j_offset)
    } else {
        value.to_string()
    }
}