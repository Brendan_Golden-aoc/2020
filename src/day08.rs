use std::collections::HashMap;

pub fn generator(input: &str) -> Vec<(&str, i32)> {
    let mut result: Vec<(&str, i32)> = Vec::new();

    for line in input.split("\n") {
        let split: Vec<&str> = line.split(" ").collect();

        let instruction = split[0];
        let value = split[1].parse::<i32>().unwrap();

        result.push( (instruction, value))
    }
    result
}

pub fn part_1(input:  &Vec<(&str, i32)>) -> i32 {
    run_instructions(input).0
}

fn run_instructions(input:  &Vec<(&str, i32)>) -> (i32, bool){
    let mut in_loop = false;

    let mut instruction_current: i32 = 0;
    let mut accumulator = 0;
    let mut instructions_ran:HashMap <i32, bool> = HashMap::new();

    loop {
        // check if out of bounds
        if instruction_current as usize >= input.len() {
            break
        }

        // check if instruction has been run
        if instructions_ran.contains_key(&instruction_current) {
            in_loop = true;
            break
        }

        instructions_ran.insert(instruction_current, true);
        let instruction = input[instruction_current as usize];

        match instruction.0 {
            "acc" => {
                accumulator += instruction.1;
                instruction_current += 1;
                continue
            },
            "jmp" => {
                instruction_current += instruction.1;
                continue
            },
            "nop" => {
                instruction_current += 1;
                continue
            },
            _ => {  }
        }


    }

    (accumulator, in_loop)

}

pub fn part_2(input:  &Vec<(&str, i32)>) -> i32 {

    // loop though all instructions
    for (position , instruction) in input.iter().enumerate() {
        let instruction_new: (&str, i32);

        match instruction.0 {
            "jmp" => {
                // change to nop
                instruction_new = ("nop", instruction.1);
            },
            "nop" => {
                // change to jmp
                instruction_new = ("jmp", instruction.1);
            },
            _ => {
                // if its not one of teh above it dosent matter
                continue
            }
        }
        let mut input_new: Vec<(&str, i32)> = Vec::new();
        input_new.extend(&input[..position]);
        input_new.push(instruction_new);
        input_new.extend(&input[(position+1)..]);

        // now test it
        let result = run_instructions(&input_new);
        if result.1 == false {
            return result.0
        }
    }

   0
}