use std::collections::HashMap;

#[derive(Debug)]
pub struct Position {
    pub row_data: Vec<String>,
    pub col_data: Vec<String>
}

pub fn input_generator(input: &str) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();

    // new line seperates passports
    for group in input.split("\n\n") {
        result.push(group.to_string())
    }
    result
}

pub fn solve_part1(input: &Vec<String>) -> i32 {
    let mut total = 0;

    for group in input {
        let flattened = group.replace("\n", "");

        let mut seen_letters = HashMap::new();
        for letter in flattened.chars() {
            if letter.is_alphabetic() {
                seen_letters.insert(letter, true);
            }
        }
        total += seen_letters.len() as i32;
    }

    total
}

pub fn solve_part2(input: &Vec<String>) -> i32 {
    let mut total = 0;

    for group in input {
        let mut seen_letters = HashMap::new();
        let mut people_count = 0;
        for person in group.split("\n") {
            people_count += 1;
            for letter in person.chars() {
                let counter = seen_letters.entry(letter).or_insert(0);
                *counter += 1;
            }
        }

        for (_, count) in &seen_letters {
            if count == &people_count {
                total += 1;
            }
        }
    }

    total
}
