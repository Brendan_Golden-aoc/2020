
#[derive(Debug)]
pub struct Instruction {
    pub action: String,
    pub movement: i32
}

pub fn generator(input: &str) -> Vec<Instruction> {
    let mut result: Vec<Instruction> = Vec::new();

    for line in input.split("\n") {
        result.push(Instruction {
            action: line[..1].to_string(),
            movement: line[1..].parse::<i32>().unwrap()
        })
    }

    result
}


pub fn part_1(instructions:  &Vec<Instruction>) -> i32 {
    // set initial state
    let mut x = 0;
    let mut y = 0;
    let mut degrees = 90;

    for instruction in instructions {
        let mut direction = &instruction.action[..];


        match direction {
            "L" => {
                degrees -= instruction.movement;
                continue
            },
            "R" => {
                degrees += instruction.movement;
                continue
            },
            _ => {}
        }


        // if its forward handle the change in cardinal direction
        if direction == "F" {
            match degrees % 360 {
                0 => { direction = "N" },
                90 | -270 => { direction = "E" },
                180  | -180 => { direction = "S" },
                270 | -90 => { direction = "W" },
                _ => { continue }
            }
        }
        // handles the actual movement
        match direction {
            "N" => {
                y += instruction.movement;
            },
            "S" => {
                y -= instruction.movement;
            },
            "E" => {
                x += instruction.movement;
            },
            "W" => {
                x -= instruction.movement;
            },
            _ => {}
        }

    }

    x.abs() + y.abs()
}


pub fn test(_unused: &Vec<Instruction>) -> i32 {
    println!("Testing");

    let input1 =
"F10
N3
F7
R90
F11";

    let generated1 = generator(input1);
    println!("{:?}", &generated1);

    let part_1_1 = part_1(&generated1);

    println!("Input 1, part 1. Expected: 25, Got: {}", part_1_1);


    0
}

