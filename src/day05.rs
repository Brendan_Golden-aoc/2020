
#[derive(Debug)]
pub struct Position {
    pub row_data: Vec<String>,
    pub col_data: Vec<String>
}

pub fn input_generator(input: &str) -> Vec<Position> {
    let mut result:  Vec<Position> = Vec::new();

    for line in input.split("\n") {
        let mut row_tmp: Vec<String> = Vec::new();
        let mut col_tmp: Vec<String> = Vec::new();

        for character in line.split("") {
            match character {
                "F" | "B" => {
                    row_tmp.push(character.to_string())
                },
                "L" | "R" => {
                    col_tmp.push(character.to_string())
                },
                _ => {}
            }
        }
        result.push(Position{
            row_data: row_tmp,
            col_data: col_tmp
        })
    }
    result
}

pub fn solve_part1(input: &Vec<Position>) -> i32 {
    // iterate through and find the two numbers that sum to 2020
    // then multiply them

    // get teh first

    let rows_array: Vec<i32> = (0..=127).collect();
    let cols_array: Vec<i32> = (0..=7).collect();


    let mut id_max = 0;

    for seat in input {
        // iterate through to tget teh second number
        let row = process_position(&seat.row_data, &rows_array);
        let col = process_position(&seat.col_data, &cols_array);

        let id = (row * 8) + col;

        if id > id_max { id_max = id}
    }

    id_max
}

fn process_position (positioning : &Vec<String>, possibilities: &Vec<i32>) -> i32{
    let length = possibilities.len();

    if length == 1 {
        return possibilities[0]
    }

    let cutoff = length/2;

    let current_position = &positioning[0] as &str;
    let next_positions: Vec<String> = positioning[1..].to_vec();

    let mut next_possibilities: Vec<i32> = possibilities.to_vec();
    match current_position {
        "F" | "L" => {
            // take the first half
            next_possibilities = next_possibilities[..cutoff].to_vec();
        },
        "B" | "R" => {
            // take the back
            next_possibilities = next_possibilities[cutoff..].to_vec();
        },
        _ => {}
    }

    return process_position(&next_positions, &next_possibilities)

}

#[derive(Debug)]
#[derive(Clone)]
pub struct Seat {
    pub row: i32,
    pub col: i32,
    pub id: i32
}

pub fn solve_part2(input: &Vec<Position>)  -> i32 {
    // iterate through and find the two numbers that sum to 2020
    // then multiply them

    // get teh first

    let rows_array: Vec<i32> = (0..=127).collect();
    let cols_array: Vec<i32> = (0..=7).collect();


    let mut seats:Vec<Seat> = Vec::new();

    for seat in input {

        // iterate through to tget teh second number
        let row = process_position(&seat.row_data, &rows_array);
        let col = process_position(&seat.col_data, &cols_array);
        let id = (row * 8) + col;

        seats.push(Seat{
            row,
            col,
            id
        })
    }

    let seats = process_seats(&seats);

    if seats.len() != 1 {
        0
    }else{
        seats[0].id
    }
}

fn process_seats(seats: &Vec<Seat>) -> Vec<Seat>{
    let mut seats_empty: Vec<Seat> = Vec::new();

   // go through each row

    // skip[ the first and last rows
    // > Your seat wasn't at the very front or back

    let mut tickets: Vec<i32> = Vec::new();

    for row in 0..=127 {
        for col in 0..=7 {
            let seat: Vec<Seat> = seats.iter()
                .filter(|&x| x.row == row)
                .filter(|&x| x.col == col)
                .cloned()
                .collect();

            let id = (row * 8) + col;
            if seat.len() == 0 {
                seats_empty.push(
                    Seat{
                        row,
                        col,
                        id
                    }
                )
            }else{
                tickets.push(id)
            }

        }
    }

    let mut seats_possible: Vec<Seat> = Vec::new();
    for possible in seats_empty {
        // the seats with an id +- 1 will be on the lis, in tickets

        let id:&i32 = &possible.id;
        let below = tickets.contains(&(id - 1));
        let above = tickets.contains(&(id + 1));

        if below && above {
            seats_possible.push(possible)
        }

    }

    seats_possible
}