
pub fn input_generator(input: &str) -> Vec<i32> {
    let mut result: Vec<i32> = Vec::new();

    for s in input.split("\n") {
        result.push( s.parse::<i32>().unwrap())
    }
    result
}

pub fn solve_part1(input: &Vec<i32>) -> i32 {
    // iterate through and find the two numbers that sum to 2020
    // then multiply them

    // get teh first
    for first in input.iter() {
        // iterate through to tget teh second number

        for second in input.iter() {
           if first + second == 2020 {
               return  first * second;
           }
        }
    }

    0
}

pub fn solve_part2(input: &Vec<i32>) -> i32 {
    // iterate through and find the two numbers that sum to 2020
    // then multiply them

    // get teh first
    for first in input.iter() {
        // iterate through to tget teh second number

        for second in input.iter() {
            // basically if the first two combined is too big then skip gettign teh third
            if first + second > 2020 {
                continue
            }

            for third in input.iter() {
                if first + second + third == 2020 {
                    return first * second * third;
                }
            }
        }
    }

    0
}